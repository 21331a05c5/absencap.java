import java.lang.*;
class AccessSpecifieDemo{
   private int priVar;
   protected int proVar;
   public int pubVar;
  
   public void setVar(int priValue, int proValue, int pubValue){
       priVar=priValue;
       proVar=proValue;
       pubVar=pubValue;
   }
   public void getVar(){
       System.out.println("\nPrivate: "+priVar);
       System.out.println("\nProtected: "+proVar);
       System.out.println("\nPublic: "+pubVar);
   }
  
   public static void main(String[] args){
       AccessSpecifieDemo obj= new AccessSpecifieDemo();
       obj.setVar(1, 2, 3);
       obj.getVar();
   }
}
